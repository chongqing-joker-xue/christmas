const startButton = document.getElementById('startButton')
const Obscuration = document.getElementById('Obscuration')
const music = document.getElementById('music')
startButton.addEventListener('click', start)

$('.enclosed').typeIt({
  whatToType: "迪丽热巴圣诞快乐, 这里有一份专属于你的圣诞礼物, 以后的每个圣诞, 我都会陪你度过...",
  typeSpeed: 100,
}, ()=>{});

let renderer, camera, scene, light, control
let width, height

function start() {
  startButton.innerHTML = '加载中...'
  Obscuration.style.display = 'block'
  play()
  initRenderer()
  initScene()
  initCamera()
  setTimeout(()=>{
    // Obscuration.style.display = 'none'
    startButton.style.display = 'none'
  },3000)
}
function play() {
  if(music.paused) {
    music.paused = false
    music.play()
  }
}

function initRenderer() {
  width = window.innerWidth
  height = window.innerHeight
  renderer = new THREE.WebGLRenderer({
    antialias: true
  })
  // 如果设置开启，允许在场景中使用阴影贴图
  // renderer.shadowMap.enabled = true
  // 定义阴影贴图类型 (未过滤, 关闭部分过滤, 关闭部分双线性过滤)
  // renderer.shadowMap.type = THREE.BasicShadowMap

  renderer.setSize(width, height)
  document.body.appendChild(renderer.domElement)
  renderer.setClearColor(0x000089, 1.0)
}

function initScene() {
  scene = new THREE.Scene()
}

function initCamera() {
  camera = new THREE.PerspectiveCamera(45, width/height, 1, 1000)
  camera.position.set(6, 2, -12)
}